USE [TEST_DB]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 5/5/2017 5:22:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[BookID] [uniqueidentifier] NOT NULL,
	[Author] [varchar](50) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Publisher] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[BookID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Book] ADD  CONSTRAINT [DF_Book_BookID]  DEFAULT (newid()) FOR [BookID]
GO
