﻿(function () {
    'use strict';

    angular
        .module('Crud')
        .factory('CRUDApi', CRUDApi);

    CRUDApi.$inject = ['$http'];

    function CRUDApi($http) {
        var protocol = "http://";
        var host = "localhost";
        var port = ":60588";

        var WebServices = {
            baseUrl: protocol + host + port + "/api/",
            Book: {
                getAllBook: function () {
                    return $http({
                        method: 'GET',
                        url: WebServices.baseUrl + 'book'
                    });
                },
                getBook: function (id) {
                    return $http({
                        method: 'POST',
                        url: WebServices.baseUrl + 'book/' + id
                    });
                },
                addBook: function (param) {
                    return $http({
                        method: 'POST',
                        url: WebServices.baseUrl + 'book',
                        data: JSON.stringify(param)
                    });
                },
                updateBook: function (param) {
                    return $http({
                        method: 'PUT',
                        url: WebServices.baseUrl + 'book/' + param.BookID,
                        data: JSON.stringify(param)
                    });
                },
                deleteBook: function (id) {
                    return $http({
                        method: 'DELETE',
                        url: WebServices.baseUrl + 'book/' + id,
                    });
                }
            }
        };

        return WebServices;
    }
})();