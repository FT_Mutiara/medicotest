﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MedicoFinalTest.Models;
using MedicoFinalTest.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MedicoFinalTest.Controllers.API
{
    [Route("api/[controller]")]
    public class BookController : Controller
    {
        private readonly BookRepository BookRepository;

        public BookController()
        {
            BookRepository = new BookRepository();
        }

        [HttpGet]
        public IEnumerable<Book> Get()
        {
            return BookRepository.GetAll();
        }

        [HttpGet("{id}")]
        public Book Get(Guid id)
        {
            return BookRepository.GetByID(id);
        }

        [HttpPost]
        public void Post([FromBody]Book book)
        {
            if (ModelState.IsValid)
                BookRepository.Add(book);
        }

        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody]Book book)
        {
            if (ModelState.IsValid)
                BookRepository.Update(book);
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            BookRepository.Delete(id);
        }
    }
}
