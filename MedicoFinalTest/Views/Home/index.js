﻿(function () {
    'use strict';

    angular
        .module('Crud')
        .controller('BookCtrl', BookCtrl);

    BookCtrl.$inject = ['$scope', 'CRUDApi'];

    function BookCtrl($scope, CRUDApi, xeditable) {
        $scope.books = [];
        CRUDApi.Book.getAllBook().then(function (response) {
            $scope.books = response.data;
        });
        $scope.saveBook = function (data) {
            if (data.BookID == '') return CRUDApi.Book.addBook(data);
            return CRUDApi.Book.updateBook(data);
        };
        $scope.addBook = function () {
            $scope.inserted = new Book();
            $scope.books.push($scope.inserted);
        };
        $scope.deleteBook = function (index,data) {
            CRUDApi.Book.deleteBook(data.BookID).then(function () {
                $scope.books.splice(index, 1);
            });
        };
    }
})();
