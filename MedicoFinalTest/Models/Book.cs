﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicoFinalTest.Models
{
    [TableName("Book")]
    [PrimaryKey("BookID")]
    public class Book
    {
        public Guid? BookID { set; get; }
        public string Author { set; get; }
        public string Title { set; get; }
        public string Publisher { set; get; }
    }
}
