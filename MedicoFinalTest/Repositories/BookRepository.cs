﻿using MedicoFinalTest.Models;
using NPoco;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MedicoFinalTest.Repositories
{
    public class BookRepository
    {
        private string ConnectionString;

        public BookRepository()
        {
            ConnectionString = @"Data Source=.;Initial Catalog=TEST_DB;User ID=sa;Password=pass@word1;";
        }
        public IDatabase Connection
        {
            get
            {
                return new Database(ConnectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public void Add(Book book)
        {
            using (IDatabase db = Connection)
            {
                book.BookID = Guid.NewGuid();
                //db.Insert<Book>(book);
                db.Insert("Book", "BookID", false, book);
            }
        }

        public IEnumerable<Book> GetAll()
        {
            using (IDatabase db = Connection)
            {
                return db.Fetch<Book>("SELECT * FROM Book");
            }
        }

        public Book GetByID(Guid id)
        {
            using (IDatabase db = Connection)
            {
                return db.SingleById<Book>(id);
            }
        }

        public void Delete(Guid id)
        {
            using (IDatabase db = Connection)
            {
                db.Delete<Book>(id);
            }
        }

        public void Update(Book book)
        {
            using (IDatabase db = Connection)
            {
                db.Update(book);
            }
        }
    }
}
